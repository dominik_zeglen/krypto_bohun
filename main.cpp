#include "iostream"
#include "string"

using namespace std;

class Matrix
{
	public:
	int x;
	int y;
	double ** elements;
	
	Matrix(const int, const int, double **);
	Matrix(const int, const int);
	~Matrix();
	
	Matrix operator+(Matrix);
	Matrix operator-(Matrix);
	Matrix operator*(double);
	Matrix operator*(Matrix);
	Matrix operator%(int);
	Matrix operator/(double);
	
	friend Matrix operator*(double, Matrix);
	
	Matrix transpose();
	Matrix inv2();
	Matrix abs();
	void print();
};

Matrix::Matrix(const int x, const int y, double ** elements)
{
	this->x = x;
	this->y = y;
	
	this->elements = new double * [x];
	
	for(int i = 0; i < this->y; i++)
		this->elements[i] = new double[y];
	
	for(int i = 0; i < x; i++)
	{
		for(int j = 0; j < y; j++)
			this->elements[i][j] = elements[i][j];
	}
}

Matrix::Matrix(const int x, const int y)
{
	this->x = x;
	this->y = y;
	
	this->elements = new double * [y];
	
	for(int i = 0; i < this->y; i++)
		this->elements[i] = new double[x];
}

Matrix Matrix::operator+(Matrix a)
{
	Matrix ret(this->x, this->y);
	
	for(int i = 0; i < this->x; i++)
	{
		for(int j = 0; j < this->y; j++)
			ret.elements[i][j] = this->elements[i][j] + a.elements[i][j];
	}
	
	return ret;
}

Matrix Matrix::operator-(Matrix a)
{
	return (*this + (-1 * a));
}

Matrix::~Matrix()
{
	;//delete[] this->elements;
}

Matrix Matrix::abs()
{
	Matrix a(this->x, this->y);
	
	for(int i = 0; i < this->y; i++)
	{
		for(int j = 0; j < this->x; j++)
		{
			if(this->elements[j][i] < 0)
				a.elements[j][i] = this->elements[j][i] * -1;
			else
				a.elements[j][i] = this->elements[j][i];
		}
	}
	
	return a;
}

Matrix Matrix::operator*(Matrix a)
{
	Matrix ret(this->x, a.y);
	for(int i = 0; i < this->x; i++)
	{
		for(int j = 0; j < a.y; j++)
		{
			ret.elements[i][j] = 0;
			
			for(int k = 0; k < this->x; k++)
				ret.elements[i][j] += this->elements[i][k] * a.elements[k][j];
		}
	}
		
	return ret;
}

Matrix operator*(double a, Matrix m)
{
	return (m * a);
}

Matrix Matrix::operator*(double a)
{
	Matrix ret(this->x, this->y);
	
	for(int i = 0; i < this->x; i++)
	{
		for(int j = 0; j < this->y; j++)
			ret.elements[i][j] = this->elements[i][j] * a;
	}
	
	return ret;
}

Matrix Matrix::operator/(double a)
{
	return (1. / a * *this);
}

Matrix Matrix::operator%(int a)
{
	Matrix ret(this->x, this->y);
	
	for(int i = 0; i < this->x; i++)
	{
		for(int j = 0; j < this->y; j++)
			ret.elements[i][j] = (int)(this->elements[i][j]) % a;
	}
	
	return ret;
}

void Matrix::print()
{
	for(int i = 0; i < this->y; i++)
	{
		cout << "| ";
		for(int j = 0; j < this->x; j++)
			cout << this->elements[i][j] << " ";
		
		cout << "|\n";
	}
}

int modInv(int a, int m)
{	
	for(int i = 0; i < m; i++)
	{
		if((a * i) % m == 1)
			return i;
	}
	
	return 0;
}

Matrix Matrix::inv2()
{
	Matrix ret(2, 2);
	
	ret.elements[0][0] = this->elements[1][1];
	ret.elements[1][0] = 26 - this->elements[1][0];
	ret.elements[0][1] = 26 - this->elements[0][1];
	ret.elements[1][1] = this->elements[0][0];
	
	double det;
	det = ((this->elements[0][0] * this->elements[1][1]) - (this->elements[1][0] * this->elements[0][1]));
	det = modInv(det, 26);
	ret = ret * det;
	
	return ret;
}

string toUpper(string s)
{
	string ret;
	ret = s;
	
	for(int i = 0; i < s.length(); i++)
	{
		if(s[i] > 96 && s[i] < 123)
			ret[i] = s[i] - 32;
	}
	
	return ret;
}

string cipher(string m, string c)
{
	string ret;
	
	for(int k = 0; k < m.length(); k += 2)
	{
		Matrix a(1, 2);
		
		for(int i = 0; i < 2; i++)
		{
			if(m[k + i] > 64 && m[k + i] < 91)
				a.elements[i][0] = m[k + i] - 65;
		}
		
		Matrix b(2, 2);
		
		for(int i = 0; i < 2; i++)
		{
			for(int j = 0; j < 2; j++)
			{
				if(c[k * 2 + i * 2 + j] > 64 && c[k * 2 + i * 2 + j] < 91)
					b.elements[i][j] = c[k * 2 + i * 2 + j] - 65;
			}
		}
		
		Matrix c(1, 2);
		c = b * a;
		c = c % 26;
		c.print();
		for(int i = 0; i < 2; i++)
		{
			if(c.elements[i][0] < 26)
				ret += c.elements[i][0] + 65;
		}
	}
	
	return ret;
}	

string decipher(string m, string c)
{
	string ret;
	
	for(int k = 0; k < m.length(); k += 2)
	{
		Matrix a(1, 2);
		
		for(int i = 0; i < 2; i++)
		{
			if(m[k + i] > 64 && m[k + i] < 91)
				a.elements[i][0] = m[k + i] - 65;
			
			if(m[k + i] > 47 && m[k + i] < 58)
				a.elements[i][0] = m[k + i] - 48 + 26;
		}
		
		Matrix b(2, 2);
		
		for(int i = 0; i < 2; i++)
		{
			for(int j = 0; j < 2; j++)
			{
				if(c[k * 2 + i * 2 + j] > 64 && c[k * 2 + i * 2 + j] < 91)
					b.elements[i][j] = c[k * 2 + i * 2 + j] - 65;
				
				if(c[k * 2 + i * 2 + j] > 47 && c[k * 2 + i * 2 + j] < 58)
					b.elements[i][j] = c[k * 2 + i * 2 + j] - 48 + 26;
			}
		}
		
		b = b.inv2();
		//b = b.abs();
		b = b % 26;
		
		Matrix c(1, 2);
		c = b * a;
		c = c % 26;
		//b.print();
		
		for(int i = 0; i < 2; i++)
		{
			if(c.elements[i][0] < 26)
				ret += c.elements[i][0] + 65;
		}
	}
	
	return ret;
}

int main()
{
	string m;
	string c;
	string x;
	cout << "Prezentacja algorytmu szyfrowania Hilla. \nProsze wprowadzic tekst do zaszyfrowania:\n";
	getline(cin, m);
	m = toUpper(m);
	cout << "Prosze wprowadzic klucz o dlugosci " << m.length() * 2 << ":\n";
	getline(cin, c);
	c = toUpper(c);
	x = cipher(m, c);
	cout << "Oto zaszyfrowany tekst:\n";
	cout << x;
	cout << "\nOto odszyfrowany tekst:\n";
	cout << decipher(x, c);
	cout << "\n";
	
	return 0;
}
